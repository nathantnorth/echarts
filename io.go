package echarts

import "encoding/json"

// ReadOptionFile reads the Option parameters from a JSON file
func ReadOptionFile(path string) (Option, error) {
	return Option{}, nil
}

// ReadOption unmarshals JSON from binary into a go struct
func ReadOption(raw []byte) (Option, error) {
	o := Option{}
	err := json.Unmarshal(raw, &o)
	return o, err
}
