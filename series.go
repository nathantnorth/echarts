package echarts

type SeriesDataAttr struct {
	ModularityClass int `json:"modularity_class"`
}

type SeriesDataLabel struct {
	Normal NormalLabelStyle `json:"normal"`
}

type SeriesLink struct {
	ID        string              `json:"id"`
	Name      string              `json:"name"`
	Source    string              `json:"source"`
	Target    string              `json:"target"`
	LineStyle SeriesLinkLineStyle `json:"lineStyle"`
}

type SeriesLinkLineStyle struct {
	Normal NormalLabelStyle `json:"normal"`
}

type Category struct {
	Name             string        `json:"name"`
	SymbolKeepAspect bool          `json:"symbolKeepAspect"`
	SymbolOffset     []interface{} `json:"symbolOffset"`
	Symbol           string        `json:"symbol"`
	SymbolSize       int           `json:"symbolSize"`
	SymbolRotate     float64       `json:"symbolRotate"`
}
