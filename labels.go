package echarts

// SeriesLabel is a label for data on a chart
type SeriesLabel struct {
	Show      bool   `json:"show"`
	Position  string `json:"position"`
	Formatter string `json:"formatter"`
	Color     string `json:"color"`
	FontSize  int    `json:"fontSize"`
}

// SeriesEmphasis defines how the labels are styled when emphasized
type SeriesEmphasis struct {
	ItemStyle ItemStyle   `json:"itemStyle"`
	LineStyle LineStyle   `json:"lineStyle"`
	Label     SeriesLabel `json:"label"`
	EdgeLabel SeriesLabel `json:"edgeLabel"`
}
