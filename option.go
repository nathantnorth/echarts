// Package echarts defines the API for the echarts javascript library for building beautiful charts
// Documentation on the chart options are here: https://www.echartsjs.com/en/option.html#title
package echarts

// Option is the highest level item in the echarts JSON object
type Option struct {
	Title                 OptionTitle   `json:"title"`
	Legend                OptionLegend  `json:"legend"`
	Tooltip               OptionTooltip `json:"tooltip"`
	AnimationDuration     int           `json:"animationDuration"`
	AnimationEasingUpdate string        `json:"animationEasingUpdate"`
	Series                []SeriesGraph `json:"series"`
}

// OptionTitle defines the structure and contents of the chart title
type OptionTitle struct {
	Show         bool      `json:"show"`
	Text         string    `json:"text"`
	Link         string    `json:"link"`   // hyperlink of the text in the title
	Target       string    `json:"target"` // 'self' or 'blank'
	TextStyle    TextStyle `json:"textStyle"`
	Subtext      string    `json:"subtext"`
	SubLink      string    `json:"sublink"`
	SubTarget    string    `json:"subtarget"`
	SubTextStyle TextStyle `json:"subtextStyle"`
	Top          string    `json:"top"`
	Left         string    `json:"left"`
	// TODO: More fields are available
}

// OptionLegend defines the contents of the chart legend
type OptionLegend struct {
	Type      string       `json:"type"`
	Show      bool         `json:"show"`
	Left      string       `json:"left"`
	Top       string       `json:"top"`
	Right     string       `json:"right"`
	Bottom    string       `json:"bottom"`
	TextStyle TextStyle    `json:"textStyle"`
	Data      []LegendData `json:"data"`
}

// LegendData defines the data within the legend
type LegendData struct {
	Name      string    `json:"name"`
	Icon      string    `json:"icon"`
	TextStyle TextStyle `json:"textStyle"`
}

// OptionTooltip sets the settings for the tooltip
type OptionTooltip struct {
	Show bool `json:"show"`
	// TODO: Lots more options here
}
