package echarts

// TextStyle defines the style of the text (based on title)
type TextStyle struct {
	Color           string `json:"color"`
	FontStyle       string `json:"fontStyle"`
	FontWeight      string `json:"fontWeight"`
	FontFamily      string `json:"fontFamily"`
	FontSize        int    `json:"fontSize"`
	LineHeight      int    `json:"lineHeight"`
	Width           string `json:"width"`
	Height          string `json:"height"`
	TextBorderColor string `json:"textBorderColor"`
	// TODO: There are more fields that are available here
}

// ItemStyle defines the style of a particular item
type ItemStyle struct {
	Color       string  `json:"color"`
	BorderColor string  `json:"borderColor"`
	BorderWidth int     `json:"borderWidth"`
	BorderType  string  `json:"borderType"`
	ShadowBlur  int     `json:"shadowBlur"`
	ShadowColor string  `json:"shadowColor"`
	Opacity     float64 `json:"opacity"`
}

// LineStyle defines the style of a line
type LineStyle struct {
	Color       string  `json:color"`
	Width       int     `json:"width"`
	Type        string  `json:"type"`
	ShadowBlur  int     `json:"shadowBlur"`
	ShadowColor string  `json:"shadowColor"`
	Opacity     float64 `json:"opacity"`
	Curveness   float64 `json:"curveness"`
}
