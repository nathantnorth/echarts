package echarts

// SeriesGraph is the graph type series
type SeriesGraph struct {
	Type               string         `json:"type"`
	Name               string         `json:"name"`
	LegendHoverLink    bool           `json:"legendHoverLink"`
	HoverAnimation     bool           `json:"hoverAnimation"`
	Layout             string         `json:"layout"`
	Force              GraphForce     `json:"force"`
	Roam               bool           `json:"roam"`
	Draggable          bool           `json:"draggable"`
	Symbol             string         `json:"symbol"`
	SymbolSize         int            `json:"symbolSize"`
	SymbolRotate       float64        `json:"symbolRotate"`
	FocusNodeAdjacency bool           `json:"focusNodeAdjacency"`
	EdgeSymbol         []string       `json:"edgeSymbol"`
	EdgeSymbolSize     []int          `json:"edgeSymbolSize"`
	ItemStyle          ItemStyle      `json:"itemStyle"`
	LineStyle          LineStyle      `json:"lineStyle"`
	Label              SeriesLabel    `json:"label"`
	EdgeLabel          SeriesLabel    `json:"edgeLabel"`
	Emphasis           SeriesEmphasis `json:"emphasis"`
	Categories         []Category     `json:"categories"`
	Data               []GraphData    `json:"data"`
	Links              []SeriesLink   `json:"links"`
}

// GraphForce defines how nodes are seperated in the graph
type GraphForce struct {
	InitLayout      string  `json:"initLayout"`
	Repulsion       int     `json:"repulsion"`
	Gravity         float64 `json:"gravity"`
	EdgeLength      int     `json:"edgeLength"`
	LayoutAnimation bool    `json:"layoutAnimation"`
}

// GraphData sets the functional data of the chart
type GraphData struct {
	Attributes SeriesDataAttr `json:"attributes"`
	Category   int            `json:"category"`
	ID         string         `json:"id"`
	Name       string         `json:"name"`
	ItemStyle  string         `json:"itemStyle"`
	SymbolSize float64        `json:"symbolSize"`
	X          float64        `json:"x"`
	Y          float64        `json:"y"`
	Value      float64        `json:"value"`
}
